<?php

namespace App\Presenters;

use App\Model\DrugModel;
use App\Model\PatientModel;
use Nette;
use App\Model;
use App\Forms\PatientFormFactory;
use Nette\Application\UI\Form;



class HomepagePresenter extends BasePresenter
{
    private $patientModel;
    private $drugModel;
    private $patientFF;


    /**
     * Injecting dependencies
     * @param PatientModel $patientModel
     * @param DrugModel $drugModel
     * @param PatientFormFactory $paetientFF
     */
    public function injectDependencies(
        PatientModel $patientModel,
        DrugModel $drugModel,
        PatientFormFactory $paetientFF
    ){
        $this->patientModel = $patientModel;
        $this->drugModel = $drugModel;
        $this->patientFF = $paetientFF;
    }

    /**
     * Render template for default page, enforces user login
     */
	public function renderDefault(){
        $this->checkLogin();
		$patients = $this->patientModel->listAll();

		if(!isset($this->template->patients)){
            $this->template->patients = $patients;
        }
	}

    /**
     * Render template for adding patients, enforces user login
     */
    public function renderAdd(){
        $this->checkLogin();
    }

    /**
     * Function for creating form for ading new patients
     * @return Form
     */
    public function createComponentAddForm()
    {
        $form = $this->patientFF->createAddForm();
        $this->formatBSForm($form);
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Záznam byl úspěšně přidán.', 'success');
            $this->redirect('Homepage:default');
        };
        return $form;
    }

    /**
     * Function for cteating search form
     * @return Form
     */
    public function createComponentSearchForm(){

        $meds = Array();
        $dbmeds = $this->drugModel->listAll();

        foreach ($dbmeds as $m){
            $meds[$m->idlek_enum]=$m->nazev;
        }

        $form = new Form();
        $form->setMethod('get');
        $form->addText('name','Jméno');
        $form->addText('lastname','Příjmení');
        $form->addText('rc','Rodné číslo');
        $form->addSelect('lecen','Aktuálně léčen',$meds)
        ->setPrompt('');
        $form->addText('kopieod','Počet kopií KG od');
        $form->addText('kopiedo','Počet kopií KG do');

        $form->addSubmit('send','Vyhledat');
        $form->onSuccess[] = array($this, 'searchFormSucceeded');

        $this->formatBSForm($form);
        return $form;
    }

    /**
     * Function for processing search form, it filters patients
     * @param $form
     * @param $values
     */
    public function searchFormSucceeded($form,$values){
        $patients = $this->patientModel->listAll();

        if($values['name']){
            $result = Array();
            foreach ($patients as $p){
                if($p->jmeno==$values['name']) {
                    $result[] = $p;
                }
            }
            $patients = $result;
        }

        if($values['lastname']){
            $result = Array();
            foreach ($patients as $p){
                if($p->prijmeni==$values['lastname']) {
                    $result[] = $p;
                }
            }
            $patients = $result;
        }

        if($values['rc']){
            $result = Array();
            foreach ($patients as $p){
                if($p->rc==$values['rc']) {
                    $result[] = $p;
                }
            }
            $patients = $result;
        }

        if($values['lecen']){
            $res = $this->patientModel->listMedicatedBy($values['lecen']);
            $result = Array();
            foreach ($patients as $p){
                if(array_search($p->idpacient,$res)!==FALSE) {
                    $result[] = $p;
                }
            }
            $patients = $result;
        }

        if($values['kopieod'] || $values['kopiedo']) {
            $res = NULL;
            if ($values['kopieod'] && $values['kopiedo']) {
                $res = $this->patientModel->listKGByInterval($values['kopieod'], $values['kopiedo']);
            } elseif ($values['kopieod'] && !$values['kopiedo']) {
                $res = $this->patientModel->listKGByInterval($values['kopieod'], NULL);
            } elseif (!$values['kopieod'] && $values['kopiedo']) {
                $res = $this->patientModel->listKGByInterval(NULL, $values['kopiedo']);
            }

            $result = Array();
            foreach ($patients as $p){
                if(array_search($p->idpacient,$res)!==FALSE) {
                    $result[] = $p;
                }
            }
            $patients = $result;
        }

        $this->template->patients = $patients;
    }

}
