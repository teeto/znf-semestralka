<?php

namespace App\Presenters;

use App\Model\UserManager;
use Nette;
use App\Forms;
use Nette\Application\ForbiddenRequestException;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;



class SignPresenter extends BasePresenter
{
	/** @var Forms\SignInFormFactory @inject */
	public $signInFactory;

	/** @var Forms\SignUpFormFactory @inject */
	public $signUpFactory;

	private $userManager;

	public function injectDependencies(UserManager $um){
	    $this->userManager = $um;
    }

    function checkLogin(){
        if (!$this->getUser()->isLoggedIn()) {
            //$this->flashMessage('Pro přístup do systému je nutné přihlášení', 'success');
            $this->redirect('Sign:in');
        }
    }

    /**
     * Checks whether user is administrator, redirects to homepage if not
     */
    protected function checkAccess(){
        $this->checkLogin();
        if (!$this->getUser()->isAllowed('usercontrol')) {
            $this->flashMessage('Pro přístup do této sekce nemáte dostatečná oprávnění.');
            $this->redirect('Homepage:default');
        }
    }

    /**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signInFactory->create(function () {
            $this->redirect('Homepage:default');

		});
	}


	/**
	 * Sign-up form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignUpForm()
	{
	    $form = $this->signUpFactory->create(function ($userMail) {

            $mail = new Message;
            $mail->setFrom('DBCML <noreply@dbcml.com>')
                ->addTo($userMail)
                ->setSubject('přístup do DBCML')
                ->setBody("Dobrý den,\nváš účet pro přístup do DBCL byl právě aktivován.");

            $mailer = new SendmailMailer;
            try {
                $mailer->send($mail);
            } catch (Nette\Mail\SendException $e){
                $this->flashMessage('Odeslání mailu s informací o vytvoření účtu se nezdařilo. Informujte uživatele osobně.');
            }

            $this->flashMessage('Účet úspěšně založen');
            $this->redirect('this');
        });

		return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl jste odhlášen');
		$this->redirect('Sign:in');
	}

	public function renderDefault(){

        $this->checkAccess();
	    $this->template->users=$this->userManager->listAll();
    }


    /**
     * Removes user from database, canot remove admin acount
     * @param $id
     */
    public function handleRemove($id){
        $this->checkLogin();
        $removedUser = $this->userManager->getUser($id);

	    if($removedUser['role'] == 'admin'){
	        $this->flashMessage('Nelze odstranit administrátorský účet');
            $this->redirect('this');
	        return;
        }

        $mail = new Message;
        $mail->setFrom('DBCML <noreply@dbcml.com>')
            ->addTo($removedUser->email)
            ->setSubject('odebrání přístupu do DBCML')
            ->setBody("Dobrý den,\nváš přístup do systému DBCML byl právě odebrán. Pro více informací kontaktujte Dr. ...");

        $mailer = new SendmailMailer;
        try {
            $mailer->send($mail);
        } catch (Nette\Mail\SendException $e){
            $this->flashMessage('Odeslání mailu s informací o odebrání přístupu se nezdařilo. Informujte uživatele osobně.');
        }

        $this->userManager->removeUser($id);
        $this->flashMessage('Účet úspěšně odebrán');
        $this->redirect('this');
    }

    /**
     * Promotes user by given user id to be administrator, has no efect on administrator acconts
     * @param $id
     */
    public function handlePromote($id){
        $this->checkLogin();
        $this->userManager->promoteUser($id);
        $this->redirect('this');
    }

}
