<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 28.03.2017
 * Time: 17:30
 */

namespace App\Presenters;

use App\Components\Patient\PatientInfoControl;
use App\Forms\MedicationFormFactory;
use App\Forms\PatientFormFactory;
use App\Forms\SampleFormFactory;
use App\Model\DrugModel;
use App\Model\MedicationModel;
use App\Model\PatientModel;
use App\Model\SampleModel;
use Nette\Forms\Form;


class PatientPresenter extends BasePresenter{

    private $patientModel;
    private $sampleModel;
    private $medicationModel;
    private $drugnModel;
    private $patientFF;
    private $sampleFF;
    private $medFF;


    /**
     * Method for injecting dependencies
     * @param SampleModel $sampleModel
     * @param PatientModel $patientModel
     * @param MedicationModel $medicationModel
     * @param DrugModel $drugModel
     * @param PatientFormFactory $patientFF
     * @param MedicationFormFactory $medFF
     * @param SampleFormFactory $sampleFF
     */
    public function injectDependencies(
        SampleModel $sampleModel,
        PatientModel $patientModel,
        MedicationModel $medicationModel,
        DrugModel $drugModel,
        PatientFormFactory $patientFF,
        MedicationFormFactory $medFF,
        SampleFormFactory $sampleFF
    ){
        $this->patientModel = $patientModel;
        $this->sampleModel = $sampleModel;
        $this->medicationModel = $medicationModel;
        $this->drugnModel = $drugModel;
        $this->patientFF = $patientFF;
        $this->sampleFF = $sampleFF;
        $this->sampleFF = $sampleFF;
        $this->medFF = $medFF;
    }

    /**
     * Render for template of patient card
     * @param $id
     */
    public function renderDefault($id){
        $this->checkLogin();
            $patient = $this->patientModel->getItem($id);
            $medication = $this->medicationModel->listAllByPatient($id);
            $samples = $this->sampleModel->listAllByPatient($id);

            $temDrugs = Array();
            $dbDrugs = $this->drugnModel->listAll();

            foreach ($dbDrugs as $d){
                $temDrugs[$d->idlek_enum] = $d->nazev;
            }

            $this->template->drugs=$temDrugs;

            $this->template->patient = $patient;
            $this->template->medication = $medication;
            $this->template->samples = $samples;
    }

    /**
     * Method for rendering template for adding a new sample
     * @param $id
     */
    public function renderAddSample($id){
        $this->checkLogin();
        $patient = $this->patientModel->getItem($id);
        $this->template->patient = $patient;
    }

    /**
     * Method for creating form for adding new sample
     * @return Form
     */
    public function createComponentAddForm()
    {
        $form = $this->sampleFF->createAddForm();
        $form['pacient_idpacient']->setDefaultValue($this->getParameter('id'));
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Záznam byl úspěšně přidán.', 'success');
            $this->redirect('this');
        };
        $this->formatBSForm($form);
        return $form;
    }


    /**
     * Method for creating form for  new medication
     * @return Form
     */
    public function createComponentMedForm()
    {
        $form = $this->medFF->createAddForm();
        $form->addHidden('pacient_idpacient')->setDefaultValue($this->getParameter('id'));
        try {
            $dbDrugs = $this->drugnModel->listAll();
            $temDrugs = Array();
            foreach ($dbDrugs as $d){
                $temDrugs[$d->idlek_enum] = $d->nazev;
            }
            $form['lek_enum_idlek_enum']->setItems($temDrugs);
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
        $form->onSuccess[] = function (Form $form) {
          //  $this->redirect('this');
            $this->flashMessage('Záznam byl úspěšně přidán.', 'success');
        };
        return $form;
    }

    /**
     * Method for creating form for ending a medication period
     * @return Form
     */
    public function createComponentEndForm(){
        $form = $this->medFF->createEndForm();
        return$form;
    }

    protected function createComponentPatientInfoControl() {
        $patientInfoControl = new PatientInfoControl($this->patientModel);
        return $patientInfoControl;
    }

    /**
     * Action for deleting a medication period
     * @param int $id
     */
    public function handleDeleteMed($idMed) {
        $this->checkLogin();
        $this->medicationModel->deleteItem($idMed);
        $this->flashMessage('Záznam byl úspěšně odebrán.', 'success');
        //$this->redirect('this');
    }

    /**
     * Action for deleting sample
     * @param $idSample
     */
    public function handleDeleteSample($idSample){
        $this->checkLogin();
        $this->sampleModel->deleteItem($idSample);
        $this->flashMessage('Záznam byl úspěšně odebrán.', 'success');
    }

    /**
     * Method for creating form for editing patient personal info
     * @return Form
     */
    public function createComponentEditPatientForm()
    {
        $p = $this->patientModel->getItem($this->getParameter('id'));
        $form = $this->patientFF->createEditForm();
        $form->setValues($p);
        if ($p->narozen) {
            $form['narozen']->setDefaultValue(date('d.m.Y', $p->narozen->getTimestamp()));
        }

        if($p->diagnoza){
            $form['diagnoza']->setDefaultValue(date('d.m.Y',$p->diagnoza->getTimestamp()));
        }

        $this->formatBSForm($form);
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Záznam byl úspěšně upraven.', 'success');
            $this->redirect('Patient:default',$this->getParameter('id'));
        };
        return $form;
    }


    /**
     * Method for rendering template for editing patient
     * @param $id
     */
    public function renderEdit($id){
        $this->checkLogin();
        $patient = $this->patientModel->getItem($id);
        $medication = $this->medicationModel->listAllByPatient($id);
        $samples = $this->sampleModel->listAllByPatient($id);

        $this->template->patient = $patient;
        $this->template->medication = $medication;
        $this->template->samples = $samples;
    }

    /**
     * Method for rendering template for editing sample
     * @param $sampleid
     * @param $patientid
     */
    public function renderEditSample($sampleid,$patientid){
        $this->checkLogin();
        $this->template->patient = $this->patientModel->getItem($patientid);
    }

    /**
     * Action for editing a sample information
     * @param $sampleid
     * @param $patientid
     */
    public function actionEditSample($sampleid,$patientid) {
        $this->checkLogin();
        $form = $this['editSampleForm'];
        try {
            $s = $this->sampleModel->getItem($sampleid);
            $form->setDefaults($s);
            $form['datum']->setDefaultValue(date('d.m.Y',$s->datum->getTimestamp()));
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Method for creating form for editing sample
     * @return Form
     */
    public function createComponentEditSampleForm()
    {
        $form = $this->sampleFF->createEditSampleForm();
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Záznam byl úspěšně upraven.', 'success');
            $this->redirect('Patient:default',$this->getParameter('patientid'));
        };
        $this->formatBSForm($form);
        return $form;
    }

}