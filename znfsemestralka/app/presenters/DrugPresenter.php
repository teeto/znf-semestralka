<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 10.04.2017
 * Time: 11:33
 */

namespace App\Presenters;


use App\Forms\DrugFormFactory;
use App\Model\DrugModel;
use App\Model\RelatedDataException;
use Nette\Application\UI\Form;

class DrugPresenter extends BasePresenter
{
    private $drugModel;
    private $drugFF;

    /**
     * Method for injecting dependencies
     * @param DrugModel $drugModel
     * @param DrugFormFactory $drugFF
     */
    public function injectDependencies(
        DrugModel $drugModel,
        DrugFormFactory $drugFF
    ){
        $this->drugModel = $drugModel;
        $this->drugFF = $drugFF;
    }

    /**
     * Render template for adding a new drug
     */
    public function renderDefault()
    {
        $this->checkLogin();
        $this->template->drugs = $this->drugModel->listAll();
    }

    /**
     * Method for creatong form for adding a new drug
     * @return mixed
     */
    public function createComponentAddForm()
    {
        $form = $this->drugFF->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Drug:default');
        };
        $this->formatBSForm($form);
        return $form;
    }

    /**
     * Method for creating form for editing an existing drug
     * @return mixed
     */
    public function createComponentEditForm()
    {
        $form = $this->drugFF->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Drug:default');
        };
        $this->formatBSForm($form);
        return $form;
    }

    /**
     * Action for editation of drug
     * @param int $id
     */
    public function handleEdit($id) {
        $this->checkLogin();
        $form = $this['editForm'];
        try {
            $drug = $this->drugModel->getItem($id);
            $this->template->drugname = $drug->nazev;
            $form->setDefaults($drug);
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Action for deleting a drug
     * @param int $id
     */
    public function handleDelete($id) {
        $this->checkLogin();
        try {
            $this->drugModel->deleteItem($id);
        } catch (RelatedDataException $e){
            $this->flashMessage('Záznam nelze odstranit, neboť je použit u existující léčby!');
        }
        $this->redirect('Drug:default');
    }

}