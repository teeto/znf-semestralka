<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 11:47
 */

namespace App\Model;

use Nette;

class NoDataFound extends \Exception {};


class BaseModel extends Nette\Object{

    protected $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

}