<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 11:51
 */

namespace App\Model;


use Nette;

class MedicationModel extends BaseCRUDModel {



    function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database, 'lecba');
    }

    /**
     * returns medication specified by id
     * @param $id
     * @return static
     */
    public function getItem($id)
    {
        $result = $this->database->table('lecba')->where('idlecba',$id);
        return $result;
    }

    /**
     * lists all medication by pacient given by id
     * @param $id
     * @return Nette\Database\Table\GroupedSelection
     */
    public function listAllByPatient($id){
        return $this->database->table('pacient')->get($id)->related('lecba');
    }

    /**
     * validates dates of medication
     * @param $values
     * @return bool
     */
    function isValid($values){
        if(isset($values['od'])){
            if(preg_match($this::$dateRegex,$values['od'])===0){
                return false;
            }
        }

        if(isset($values['do'])){
            if(preg_match($this::$dateRegex,$values['do'])===0){
                return false;
            }
        }

        return true;
    }
}