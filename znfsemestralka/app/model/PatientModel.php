<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 11:49
 */

namespace App\Model;


use Nette;

class PatientModel extends BaseCRUDModel{

    function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database, 'pacient');
    }

    /**
     * lists patients medicated by drug specified by id
     * @param $idMed
     * @return array
     */
    function listMedicatedBy($idMed){
        $meds =  $this->database->table('lecba')->where('lek_enum_idlek_enum = ?',$idMed)->fetchAll();

        $result = Array();
        foreach ( $meds as $m){
            $result[] = $m->ref('pacient')->idpacient;
        }

        return $result;
    }

    /**
     * lists patients with samples KG value within given interval
     * @param null $kgFrom
     * @param null $kgTo
     * @return array
     */
    function listKGByInterval($kgFrom=NULL, $kgTo=NULL)
    {
        $samples = NULL;
        /*echo "HLEDAM  > $kgFrom | < $kgTo";
        var_dump($kgFrom);
        var_dump($kgTo);*/
        if ($kgFrom && ($kgTo == NULL)) {
            //echo "OD++";
            $samples = $this->database->table('odber')->where('kopie >= ?', $kgFrom)->fetchAll();
        } else if (($kgFrom == NULL) && $kgTo) {
            ////echo "DO++";
            $samples = $this->database->table('odber')->where('kopie <= ?', $kgTo)->fetchAll();
        } else if ($kgFrom && $kgTo) {
            //echo "OBOJE";
            $samples = $this->database->table('odber')->where('kopie >= ? AND kopie <= ?', $kgFrom, $kgTo)->fetchAll();
        }


        $result = Array();
        if ($samples) {

            foreach ($samples as $s) {
                $result[] = $s->ref('pacient')->idpacient;
            }
        }
        //var_dump($result);
        return $result;
    }

    /**
     * Validates values
     * @param $values
     * @return bool
     */
    function isValid($values)
    {
        if(isset($values['jmeno'])){
            if(preg_match($this::$nameRegex,$values['jmeno'])===0){
                return false;
            }
        }

        if(isset($values['prijmeni'])){
            if(preg_match($this::$nameRegex,$values['prijmeni'])===0){
                return false;
            }
        }

        if(isset($values['rc'])){
            if(preg_match($this::$numberRegex,$values['rc'])===0){
                return false;
            }
        }

        if(isset($values['diagnoza'])){
            if(preg_match($this::$dateRegex,$values['diagnoza'])===0){
                return false;
            }
        }

        if(isset($values['narozen'])){
            if(preg_match($this::$dateRegex,$values['narozen'])===0){
                return false;
            }
        }


        return true;
    }
}