<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 12:09
 */

namespace App\Model;


use Nette;

class SampleModel extends BaseCRUDModel {

    function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database, 'odber');
    }

    /**
     * returns sample specified by id
     * @param $id
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getItem($id)
    {
        $result = $this->database->table('odber')->where('idodber = ?',$id)->fetch();
        return $result;
    }

    /**
     * returns all samples of patient by given patient id
     * @param $id
     * @return Nette\Database\Table\GroupedSelection
     */
    public function listAllByPatient($id){
        return $this->database->table('pacient')->get($id)->related('odber');
    }

    /**
     * validates values for sample
     * @param $values
     * @return bool
     */
    function isValid($values)
    {
        if(isset($values['kopie'])){
            if(preg_match($this::$numberRegex,$values['kopie'])===0){
                return false;
            }
        }

        if(isset($values['datum'])) {
            if (preg_match($this::$dateRegex, $values['datum']) === 0) {
                return false;
            }
        }

        return true;
    }
}