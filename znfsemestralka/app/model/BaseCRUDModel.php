<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 12:21
 */

namespace App\Model;

use Nette;

class NoTableSpecified extends \Exception {};

class RelatedDataException extends \Exception {};

class ValidationException extends  \Exception {};


abstract class BaseCRUDModel extends BaseModel {

    /**
     * @var string given by concrete implementation
     */
    protected $table=null;

    /**
     * @var string regex for MySql date validation
     */
    public static $dateRegex = '/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/';

    /**
     * @var string regex for only-text valivation
     */
    public static $nameRegex = '/^[a-zA-Zá-žÁ-Ž ]+$/';

    /**
     * @var string regex for numbers validation
     */
    public static $numberRegex = '/^[0-9]+$/';

    function __construct(Nette\Database\Context $database, $tableName)
    {
        parent::__construct($database);
        $this->table=$tableName;
    }

    /**
     * function for checkicking whether database table is specified by concrete implementation of the class
     * @throws NoTableSpecified
     */
    private function tableCheck(){
        if(!$this->table){
            throw new NoTableSpecified();
        }
    }

    /**
     * lists all entries
     * @return array|Nette\Database\Table\IRow[]|Nette\Database\Table\Selection
     */
    function listAll(){
        return $this->database->table($this->table)->fetchAll();
    }

    /**
     * return item from database specified by given id
     * @param $id
     * @return Nette\Database\Table\IRow
     * @throws NoDataFound
     */
    function getItem($id){
        $result =  $this->database->table($this->table)->get($id);
        if(!$result){
            throw new NoDataFound('Data not found for table '.$this->table);
        }
        return $result;
    }

    /**
     * method for inserting item to databse, checks validation firsts
     * @param $values
     * @throws ValidationException
     */
    function insertItem($values){
        if($this->isValid($values)){
            $this->database->table($this->table)->insert($values);
        }else {
            throw new ValidationException();
        }
    }

    /**
     * method for updating item in database, checks validation first
     * @param $id
     * @param $values
     * @throws ValidationException
     */
    function updateItem($id,$values){
        if($this->isValid($values)){
            $this->getItem($id)->update($values);
        }else {
            throw new ValidationException();
        }
    }

    /**
     * method for deleting item from database
     * @param $id
     */
    function deleteItem($id){
        $this->getItem($id)->delete();
    }

    /**
     * abstract method for validating values, must be implemented by concrete implementation of this abstract class
     * @param $values
     * @return mixed
     */
    abstract function isValid($values);
}