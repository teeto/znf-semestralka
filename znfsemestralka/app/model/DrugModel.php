<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 27.03.2017
 * Time: 12:08
 */

namespace App\Model;


use Nette;

class DrugModel extends BaseCRUDModel {

    function __construct(Nette\Database\Context $database)
    {
        parent::__construct($database, 'lek_enum');
    }

    /**
     * Removes drug from database only if drug id not udes in any existing medication
     * @param $id
     * @throws RelatedDataException
     */
    public function deleteItem($id)
    {
        $d = $this->getItem($id);

        if(count($d->related('lecba'))){
            throw new RelatedDataException();
        }
        parent::deleteItem($id);
    }

    /**
     * Validates drug name, there aro no rules for drug name
     * @param $values
     * @return bool
     */
    function isValid($values)
    {
        return true;
    }
}