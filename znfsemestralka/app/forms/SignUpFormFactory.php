<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;


class SignUpFormFactory
{
	use Nette\SmartObject;

	const PASSWORD_MIN_LENGTH = 7;

	/** @var FormFactory */
	private $factory;

	/** @var Model\UserManager */
	private $userManager;


	public function __construct(FormFactory $factory, Model\UserManager $userManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->addText('username', 'Uživatelské jméno:')
			->setRequired('Zvolte uživatelské jméno.');

		$form->addEmail('email', 'E-mail:')
			->setRequired('Zadejte e-mail.');

		$form->addPassword('password', 'Zadejte heslo:')
			->setOption('description', sprintf('minimálně %d znaků', self::PASSWORD_MIN_LENGTH))
			->setRequired('Vyplňte heslo.')
			->addRule($form::MIN_LENGTH, NULL, self::PASSWORD_MIN_LENGTH);
		$role = Array();
		$role['user'] = 'User';
		$role['admin'] = 'Admin';
		$form->addSelect('role','Vyberte roli:',$role);

		$form->addSubmit('send', 'Založit účet');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->userManager->add($values->username, $values->email, $values->password, $values->role);
			} catch (Model\DuplicateNameException $e) {
				$form['username']->addError('Uživatelské jméno již zabráno.');
				return;
			}
			$onSuccess($values->email);
		};

		return $form;
	}

}
