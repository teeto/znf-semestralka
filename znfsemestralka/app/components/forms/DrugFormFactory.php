<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 13.05.2017
 * Time: 1:38
 */

namespace App\Forms;


use App\Model\DrugModel;
use Nette\Forms\Container;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class DrugFormFactory{

    private $drugModel;

    public function injectDependencies(DrugModel $drugModel){
        $this->drugModel=$drugModel;
    }

    protected function addCommonFields(Container &$form, $args = null){
        $form->addText('nazev', 'Název léku')
            ->setAttribute('placeholder', 'Vyplňte název')
            ->setRequired('Je třeba vyplnit název');
    }

    public function createAddForm($args = null){
        $form = new Form(NULL, 'addForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Vytvořit záznam');
        $form->onSuccess[] = [$this, "newFormSucceeded"];
        return $form;
    }

    public function createEditForm($args = null){
        $form = new Form(NULL, 'editForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Aktualizovat záznam');
        $form->addHidden('idlek_enum');
        $form->onSuccess[] = [$this, "editFormSucceeded"];
        return $form;
    }

    public function newFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $this->drugModel->insertItem($values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    public function editFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $id = $values['idlek_enum'];
            unset($values['idlek_enum']);
            $this->drugModel->updateItem($id, $values);
        } catch (Exception $exception){
            $form->addError($exception);
        }
    }

}