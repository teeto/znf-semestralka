<?php
namespace App\Forms;

use App\Model\SampleModel;

use App\Utils\Utils;
use Nette\Forms\Container;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 15.04.2017
 * Time: 14:59
 */
class SampleFormFactory extends \Nette\Object{

    private $sampleModel;
    private $utils;

    public function injectDependencies( SampleModel $sampleModel, Utils $utils ) {
        $this->sampleModel = $sampleModel;
        $this->utils=$utils;
    }

    protected function addCommonFields(Container &$form, $args = null){
        $form->addHidden('pacient_idpacient');
        $form->addText('datum', 'Datum odběru')
            ->setAttribute('placeholder', 'Vyplňte datum odběru - formát DD.MM.RRRR')
            ->setRequired("Je třeba vyplnit datum odběru")
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
        $form->addText('kopie', 'Počet kopií kontrolního genu')
            ->setAttribute('placeholder', 'Vyplňte počet kopií')
            ->setRequired('Je třeba vyplnit počet kopií')
            ->addRule(Form::INTEGER, 'Počet kopií KG musí být číslo');
        $form->addTextArea('poznamka', 'Poznámka')
            ->setRequired(FALSE);
    }

    public function createAddForm($args = null){
        $form = new Form(NULL, 'addForm');
        //$form->getElementPrototype()->setAttribute('novalidate',TRUE);
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Vytvořit záznam');
        $form->onSuccess[] = [$this, "newFormSucceeded"];
        return $form;
    }

    public function createEditSampleForm($args = null){
        $form = new Form(NULL, 'editSampleForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Aktualizovat záznam');
        $form->addHidden('idodber');
        $form->onSuccess[] = [$this, "editFormSucceeded"];
        return $form;
    }

    public function createDeleteForm($args = null)
    {
        $form = new Form(NULL, 'deleteForm');
        $form->addProtection('Ochrana');
        $form->addSubmit('send', 'Odebrat záznam');
        $form->addHidden('idodber');
        $form->onSuccess[] = [$this, "deleteFormSucceeded"];
        return $form;
    }

    public function newFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $values['datum']=date('Y-m-d', strtotime($values['datum']));
            $this->sampleModel->insertItem($values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    public function editFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $id = $values['idodber'];
            unset($values['idodber']);
            $values['datum']=date('Y-m-d', strtotime($values['datum']));
            $this->sampleModel->updateItem($id, $values);
        } catch (Exception $exception){
            $form->addError($exception);
        }
    }

    public function deleteFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $values['datum']=date('Y-m-d', strtotime($values['datum']));
            $this->sampleModel->deleteItem($values['idodber']);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

}