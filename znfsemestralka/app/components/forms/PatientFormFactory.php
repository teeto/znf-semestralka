<?php
namespace App\Forms;

use App\Model\PatientModel;

use App\Utils\Utils;
use Nette\Forms\Container;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use App\Forms\BSFormater;
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 15.04.2017
 * Time: 14:08
 */
class PatientFormFactory extends BaseFormFactory {

    private $patientModel;
    private $utils;

    public function injectDependencies( PatientModel $patientModel, Utils $utils ) {
        $this->patientModel = $patientModel;
        $this->utils=$utils;
    }


    protected function addCommonFields(Container &$form, $args = null){
        $form->addText('jmeno', 'Jméno')
            ->setAttribute('placeholder', 'Vyplňte jméno')
            ->setRequired('Je třeba vyplnit jméno')
            ->addRule(FORM::PATTERN, 'Neplatný formát jména', '[a-zA-Zá-ž ]+');
        $form->addText('prijmeni', 'Příjmení')
            ->setAttribute('placeholder', 'Vyplňte příjmení')
            ->setRequired('Je třeba vyplnit příjmení')
            ->addRule(FORM::PATTERN, 'Neplatný formát příjmení', '[a-zA-Zá-ž ]+');
        $form->addText('rc', 'Rodné číslo')
            ->setAttribute('placeholder', 'Vyplňte rodné číslo - pouze čísla')
            ->setRequired('Je třeba vyplnit rč')
            ->addRule(Form::INTEGER, 'RČ číslo musí být číslo');
        $form->addText('narozen', 'Datum narození')
            ->setAttribute('placeholder', 'Vyplňte datum narození - formát DD.MM.RRRR')
            ->setRequired(FALSE)
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
        $form->addText('diagnoza', 'Datum diagnózy')
            ->setAttribute('placeholder', 'Vyplňte datum diagnózy - formát DD.MM.RRRR')
            ->setRequired(FALSE)
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
    }

    public function createAddForm($args = null){
        $form = new Form(NULL, 'addForm');
        // = $this->formatForm($form);
        //$form->getElementPrototype()->setAttribute('novalidate',TRUE);
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Vytvořit záznam');
        $form->onSuccess[] = [$this, "newFormSucceeded"];
        return $form;
    }

    public function createEditForm($args = null){
        $form = new Form(NULL, 'editForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Aktualizovat záznam');
        $form->addHidden('idpacient');
        $form->onSuccess[] = [$this, "editFormSucceeded"];
        return $form;
    }

    public function newFormSucceeded(Form $form, ArrayHash $values)
    {
        try {

            if($values['narozen']!=""){
                $values['narozen']=date('Y-m-d', strtotime($values['narozen']));
            }else{
                unset($values['narozen']);
            }

            if($values['diagnoza']!=""){
                $values['diagnoza']=date('Y-m-d', strtotime($values['diagnoza']));
            }else{
                unset($values['diagnoza']);
            }

            $this->patientModel->insertItem($values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    public function editFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $id = $values['idpacient'];
            unset($values['idpacient']);

            if($values['narozen']!=""){
                $values['narozen']=date('Y-m-d', strtotime($values['narozen']));
            }else{
                unset($values['narozen']);
            }

            if($values['diagnoza']!=""){
                $values['diagnoza']=date('Y-m-d', strtotime($values['diagnoza']));
            }else{
                unset($values['diagnoza']);
            }

            $this->patientModel->updateItem($id, $values);
        } catch (Exception $exception){
            $form->addError($exception);
        }
    }
}