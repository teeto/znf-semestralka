<?php
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 13.05.2017
 * Time: 14:10
 */

namespace App\Forms;

use App\Model\DrugModel;
use App\Model\MedicationModel;
use Nette\Forms\Container;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class MedicationFormFactory{

    private $medicationModel;

    public function injectDependencies(MedicationModel $medModel){
        $this->medicationModel=$medModel;
    }

    protected function addCommonFields(Container &$form, $args = null){
        $form->addSelect('lek_enum_idlek_enum', 'Vyberte lék');
        $form->addText('od', 'Léčen od')
            ->setRequired(TRUE)
            ->setDefaultValue(date("d.m.Y"))
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
        $form->addText('do', 'Léčen do')
            ->setRequired(FALSE)
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
    }

    public function createAddForm($args = null){
        $form = new Form(NULL, 'addForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Vytvořit záznam');
        $form->onSuccess[] = [$this, "newFormSucceeded"];
        return $form;
    }

    public function createEditForm($args = null){
        $form = new Form(NULL, 'editForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Aktualizovat záznam');
        $form->addHidden('idlecba');
        $form->onSuccess[] = [$this, "editFormSucceeded"];
        return $form;
    }

    public function createEndForm($args=null){
        $form = new Form(NULL, 'endForm');
        $form->addProtection('Ochrana');
        $form->addText('do', 'Léčen do')
            ->setRequired(TRUE)
            ->setDefaultValue(date("d.m.Y"))
            ->addRule(Form::PATTERN, 'Formád data DD.MM.RRRR', '(31|30|[12][0-9]|0?[1-9])\.\s*(12|11|10|0?[1-9])\.\s*[1-9][0-9]{3}');
        $form->addHidden('idlecba');
        $form->addSubmit('send', 'Ukončit');
        $form->onSuccess[] = [$this, "endFormSucceeded"];
        return $form;
    }

    public function newFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $values['od']=date('Y-m-d', strtotime($values['od']));
            if($values['do']!=""){
                $values['do']=date('Y-m-d', strtotime($values['do']));
            }else{
                unset($values['do']);
            }
            $this->medicationModel->insertItem($values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    public function editFormSucceeded(Form $form, ArrayHash $values)
    {
       /* try {
            $id = $values['idlek_enum'];
            unset($values['idlek_enum']);
            $this->drugModel->updateItem($id, $values);
        } catch (Exception $exception){
            $form->addError($exception);
        }*/
    }

    public function endFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $values['do']=date('Y-m-d', strtotime($values['do']));
            $id = $values['idlecba'];
            unset($values['idlecba']);
            $this->medicationModel->updateItem($id, $values);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

}