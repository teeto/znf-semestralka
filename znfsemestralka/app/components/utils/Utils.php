<?php

namespace App\Utils;
use Nette\Object;

/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 15.04.2017
 * Time: 18:20
 */
class Utils extends Object {

    /**
     * Fonction converts given EU date format to MySql
     * @param $date
     * @return string
     */
    public function dateToMysql ($date){
        $arrDate = explode('.',$date);
        $day=$arrDate[0];
        $month=$arrDate[1];
        $year=$arrDate[2];

        return $year.'-'.$month.'-'.$day;
    }

    /**
     * Function converts date to MySQL format, returns null if no date is given
     * @param $date
     * @return null|string
     */
    public function extractDate($date){
        if($date==''){
            return null;
        }

        return $this->dateToMysql($date);
    }

}