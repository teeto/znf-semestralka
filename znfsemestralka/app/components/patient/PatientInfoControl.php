<?php

namespace App\Components\Patient;

use App\Model\PatientModel;
use Nette\Application\UI\Control;

/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 29.05.2017
 * Time: 17:20
 */
class PatientInfoControl extends Control
{

    /**
     * Model pro pacienta
     * @var
     */
    private $patientModel;


    /**
     * PatientInfoControl constructor. Seter for patient model
     * @param StatisticModel $sm
     */
    public function __construct( PatientModel $sm )
    {
        $this->patientModel = $sm;
    }


    /**
     * Method for rendering component, required id of patient
     * @param $id
     */
    public function render($id)
    {

        $template = $this->template;
        $template->setFile(__DIR__ . '/PatientInfoControl.latte');
        $template->patient = $this->patientModel->getItem($id);
        $template->render();
    }
}