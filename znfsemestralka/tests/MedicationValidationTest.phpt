<?php

use Tester\Assert;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Structure;

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../app/model/BaseModel.php';
require __DIR__ . '/../app/model/BaseCRUDModel.php';
require __DIR__ . '/../app/model/MedicationModel.php';

require 'config.php';


Tester\Environment::setup();

$connection = new Connection($dns, $user, $passwd);
$database = new Context($connection, new Structure($connection, new DevNullStorage()));

$o = new \App\Model\MedicationModel($database);

// v medikaci je zajímavé pouze datum, které zde testuji

$values['od']='abcd';
Assert::same( false, $o->isValid($values) );

$values['od']='02042005';
Assert::same( false, $o->isValid($values) );

$values['od']='2015-10-10';
Assert::same( true, $o->isValid($values) );
