<?php

use Tester\Assert;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Structure;

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../app/model/BaseModel.php';
require __DIR__ . '/../app/model/BaseCRUDModel.php';
require __DIR__ . '/../app/model/PatientModel.php';
require 'config.php';




Tester\Environment::setup();

$connection = new Connection($dns, $user, $passwd);
$database = new Context($connection, new Structure($connection, new DevNullStorage()));

$o = new \App\Model\PatientModel($database);

// datové atributy lze testovat v testu medikace, zde se zaměřím na textové pole pro jméno, příjmení a číselnou hodnotu rodného čísla
// z důvodu výskytu pacientů z různých národností nejsou kladeny nároky na rodné číslo kromě numerické hodnoty

$values['jmeno']='abcd';
$values['prijmeni']='abcd';
$values['rc']='abcd';
Assert::same( false, $o->isValid($values) );

$values['jmeno']='Tomas';
$values['prijmeni']='Pastorek';
$values['rc']='123456789';
Assert::same( true, $o->isValid($values) );

$values['jmeno']='Tomáš';
$values['prijmeni']='Čejka';
$values['rc']='123456789';
Assert::same( true, $o->isValid($values) );

$values['jmeno']='Tomáš25';
$values['prijmeni']='Čejka';
$values['rc']='123456789';
Assert::same( false, $o->isValid($values) );

$values['jmeno']='Tomáš';
$values['prijmeni']='Če55jka';
$values['rc']='123456789';
Assert::same( false, $o->isValid($values) );

$values['jmeno']='Tomáš';
$values['prijmeni']='Čejka';
$values['rc']='abcd';
Assert::same( false, $o->isValid($values) );

$values['jmeno']='Tomáš';
$values['prijmeni']='Čejka';
$values['rc']='456 88797';
Assert::same( false, $o->isValid($values) );
